import re
from pprint import pprint


def convert(s):
    a = re.compile("((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))")
    return a.sub(r"_\1", s).lower()


def convertJSON(j):
    out = {}
    for k in j:
        newK = convert(k)
        if isinstance(j[k], dict):
            out[newK] = convertJSON(j[k])
        elif isinstance(j[k], list):
            out[newK] = convertArray(j[k])
        else:
            out[newK] = j[k]
    return out


def convertArray(a):
    newArr = []
    for i in a:
        if isinstance(i, list):
            newArr.append(convertArray(i))
        elif isinstance(i, dict):
            newArr.append(convertJSON(i))
        else:
            newArr.append(i)
    return newArr


data = {
    "nextPage": "fake_next_page",
    "data": [
        {
            "companyKey": "Fake",
            "authenticationCode": "Fake",
            "channel": "INTERNAL",
            "status": "DONE",
            "createdAt": "2022-05-04T18:06:37.856-03:00",
            "updatedAt": "2022-05-04T18:06:39.378-03:00",
        },
        {
            "companyKey": "Fake1",
            "authenticationCode": "Fake2",
            "sender": {
                "documentNumber": "12345678901",
                "name": "João Guilherme Silveira",
            },
            "channel": "INTERNAL",
            "status": "DONE",
            "createdAt": "2022-05-04T18:06:07.491-03:00",
            "updatedAt": "2022-05-04T18:06:09.194-03:00",
        },
    ],
}


person = [{"firstName": "kelvinAlisson", "lastName": "Cantarino"}]

output = convertJSON(data)

pprint(output)
